/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "BRedProcessor.h"

//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */
class MainContentComponent : public AudioAppComponent, public FileDragAndDropTarget
{
public:
  //==============================================================================
  MainContentComponent()
  : mEditor( mProcessor.createEditorIfNeeded() )
  {
    mFormatMgr.registerBasicFormats();
    addAndMakeVisible(mEditor);
    
    setSize(500, 300);
    
    // specify the number of input and output channels that we want to open
    setAudioChannels (2, 2);
  }
  
  ~MainContentComponent()
  {
    mProcessor.editorBeingDeleted(mEditor);
    mEditor = nullptr;
    
    shutdownAudio();
  }
  
  //==============================================================================
  void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override
  {
    if( mSource )
    {
      mSource->prepareToPlay(samplesPerBlockExpected, sampleRate);
    }
  }
  
  void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override
  {
    // Clear the buffer (to prevent the output of random noise)
    bufferToFill.clearActiveBufferRegion();

    if( mSource )
    {
      mSource->getNextAudioBlock(bufferToFill);
      mProcessor.processBlock(*bufferToFill.buffer, mMidiBuffer);
    }
  }
  
  void releaseResources() override
  {
    mProcessor.releaseResources();
    if( mSource )
    {
      mSource->releaseResources();
    }
  }
  
  //==============================================================================
  void paint (Graphics& g) override
  {
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (Colours::black);
    
    
    // You can add your drawing code here!
  }
  
  void resized() override
  {
    if( mEditor )
    {
      mEditor->setBounds(0, 0, getWidth(), getHeight());
    }
  }
  
  
private:
  
  /// FileDragAndDropTarget methods
  virtual bool isInterestedInFileDrag (const StringArray& files) override
  {
    if(files.isEmpty())
      return false;
    
    // hasFileExtension() takes a string like .wav;.mp3
    // but getWildcardForAllFormats() returns *.wav;*.mp3 :
    const auto exts = mFormatMgr.getWildcardForAllFormats().removeCharacters("*");
    const File dropped( files[0] );
    if( dropped.existsAsFile() && dropped.hasFileExtension(exts) )
    {
      return true;
    }
    return false;
  }
  
  virtual void filesDropped (const StringArray& files, int x, int y) override
  {
    const File dropped( files[0] );
    auto * reader = mFormatMgr.createReaderFor(dropped);
    mSource = new AudioFormatReaderSource(reader, true);
  }
  
private:
  AudioFormatManager mFormatMgr;
  ScopedPointer<AudioFormatReaderSource> mSource;
  BRedProcessor mProcessor;
  ScopedPointer<AudioProcessorEditor> mEditor;
  MidiBuffer mMidiBuffer;
  
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


// (This function is called by the app startup code to create our main component)
Component* createMainContentComponent()     { return new MainContentComponent(); }


#endif  // MAINCOMPONENT_H_INCLUDED
