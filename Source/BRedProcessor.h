/*
  ==============================================================================

    BRedProcessor.h
    Created: 11 Apr 2016 6:57:44pm
    Author:  Adrien Anselme

  ==============================================================================
*/

#ifndef BREDPROCESSOR_H_INCLUDED
#define BREDPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class BRedProcessor : public AudioProcessor
{
public:
  
  BRedProcessor();
  virtual ~BRedProcessor();
  
  virtual const String getName() const;
  virtual void prepareToPlay (double sampleRate,
                              int estimatedSamplesPerBlock);
  
  virtual void releaseResources();
  
  virtual void processBlock (AudioBuffer<float>& buffer,
                             MidiBuffer& midiMessages);
  
  virtual double getTailLengthSeconds() const;
  
  virtual bool acceptsMidi() const;
  virtual bool producesMidi() const;
  virtual AudioProcessorEditor* createEditor();
  virtual bool hasEditor() const;
  virtual int getNumPrograms();
  virtual int getCurrentProgram();
  virtual void setCurrentProgram (int index);
  virtual const String getProgramName (int index);
  virtual void changeProgramName (int index, const String& newName);
  virtual void getStateInformation (juce::MemoryBlock& destData);
  virtual void setStateInformation (const void* data, int sizeInBytes);
  
private:
  AudioProcessorValueTreeState mState;
  
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(BRedProcessor);
};


#endif  // BREDPROCESSOR_H_INCLUDED
