/*
 ==============================================================================
 
 BRedProcessor.cpp
 Created: 11 Apr 2016 6:57:44pm
 Author:  Adrien Anselme
 
 ==============================================================================
 */

#include "BRedProcessor.h"
#include "BRedProcessorEditor.h"

BRedProcessor::BRedProcessor()
: mState(*this, nullptr)
{
  mState.createAndAddParameter("outputGain", "Output Gain", "%",
                               NormalisableRange<float>(.0f, 1.0f, 0.01f), 1.0f,
                               [](float x){ return String(x); },
                               [](const String& s){ return s.getFloatValue(); } );
  
  mState.createAndAddParameter("bitDepth", "Bits per sample", "",
                               NormalisableRange<float>(1.0f, 16.0f, 1.0f), 16.0f,
                               [](float x){ return String(x); },
                               [](const String& s){ return s.getFloatValue(); } );
}

BRedProcessor::~BRedProcessor()
{
  
}

const String BRedProcessor::getName() const
{
  return "BRed";
}

void BRedProcessor::prepareToPlay(double sampleRate,
                                  int estimatedSamplesPerBlock)
{
  
}

void BRedProcessor::releaseResources()
{
  
}

void BRedProcessor::processBlock(AudioBuffer<float>& buffer,
                                 MidiBuffer& midiMessages)
{
  float * pBitDepth = mState.getRawParameterValue("bitDepth");
  jassert(pBitDepth != nullptr);
  const float max = powf(2.0f, *pBitDepth) - 1;
  for(int sample = 0; sample < buffer.getNumSamples(); ++sample)
  {
    for(int chan = 0; chan < buffer.getNumChannels(); ++chan)
    {
      const float oldValue = buffer.getSample(chan, sample);
      const float newValue = roundToInt(oldValue * max) / max;
      buffer.setSample(chan, sample, newValue);
    }
  }
  
  float * pGain = mState.getRawParameterValue("outputGain");
  jassert(pGain != nullptr);
  buffer.applyGain(0, buffer.getNumSamples(), *pGain);
}

double BRedProcessor::getTailLengthSeconds() const
{
  return 0.0;
}

bool BRedProcessor::acceptsMidi() const
{
  return false;
}

bool BRedProcessor::producesMidi() const
{
  return false;
}

AudioProcessorEditor* BRedProcessor::createEditor()
{
  return new BRedProcessorEditor(this, mState);
}

bool BRedProcessor::hasEditor() const
{
  return true;
}

int BRedProcessor::getNumPrograms()
{
  return 1;
}

int BRedProcessor::getCurrentProgram()
{
  return 0;
}

void BRedProcessor::setCurrentProgram(int index)
{
  
}

const String BRedProcessor::getProgramName(int index)
{
  return "Default";
}

void BRedProcessor::changeProgramName(int index, const String& newName)
{
  
}

void BRedProcessor::getStateInformation(juce::MemoryBlock& destData)
{
  
}

void BRedProcessor::setStateInformation(const void* data, int sizeInBytes)
{
  
}

