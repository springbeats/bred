/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.2.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_35D65AE74439F1F8__
#define __JUCE_HEADER_35D65AE74439F1F8__

//[Headers]     -- You can add your own extra header files here --
#include "../JuceLibraryCode/JuceHeader.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class BRedProcessorEditor  : public AudioProcessorEditor
{
public:
    //==============================================================================
    BRedProcessorEditor (AudioProcessor* processor, AudioProcessorValueTreeState& state);
    ~BRedProcessorEditor();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;



private:
    //[UserVariables]   -- You can add your own custom variables in this section.
  OwnedArray<AudioProcessorValueTreeState::SliderAttachment> mSlAttachments;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<Label> mTitleLbl;
    ScopedPointer<Slider> mSlGain;
    ScopedPointer<Slider> mSlBitDepth;
    ScopedPointer<Label> mDropLbl;
    ScopedPointer<Label> mBitDepthLbl;
    ScopedPointer<Label> mOutputGainLbl;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BRedProcessorEditor)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

#endif   // __JUCE_HEADER_35D65AE74439F1F8__
