/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 4.2.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "BRedProcessorEditor.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
BRedProcessorEditor::BRedProcessorEditor (AudioProcessor* processor, AudioProcessorValueTreeState& state)
    : AudioProcessorEditor(processor)
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    addAndMakeVisible (mTitleLbl = new Label (String(),
                                              TRANS("BRed - Bit Reducer")));
    mTitleLbl->setFont (Font (30.00f, Font::bold));
    mTitleLbl->setJustificationType (Justification::centred);
    mTitleLbl->setEditable (false, false, false);
    mTitleLbl->setColour (Label::backgroundColourId, Colour (0xff4a4a4a));
    mTitleLbl->setColour (Label::textColourId, Colours::white);
    mTitleLbl->setColour (TextEditor::textColourId, Colours::black);
    mTitleLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (mSlGain = new Slider ("gainSlider"));
    mSlGain->setTooltip (TRANS("Output gain %"));
    mSlGain->setRange (0, 10, 0);
    mSlGain->setSliderStyle (Slider::RotaryVerticalDrag);
    mSlGain->setTextBoxStyle (Slider::TextBoxBelow, true, 80, 20);

    addAndMakeVisible (mSlBitDepth = new Slider ("bitDepthSlider"));
    mSlBitDepth->setTooltip (TRANS("Bits per sample"));
    mSlBitDepth->setRange (0, 10, 0);
    mSlBitDepth->setSliderStyle (Slider::RotaryVerticalDrag);
    mSlBitDepth->setTextBoxStyle (Slider::TextBoxBelow, true, 80, 20);

    addAndMakeVisible (mDropLbl = new Label (String(),
                                             TRANS("Drop a file in this window to play!")));
    mDropLbl->setFont (Font (Font::getDefaultSansSerifFontName(), 20.00f, Font::italic));
    mDropLbl->setJustificationType (Justification::centred);
    mDropLbl->setEditable (false, false, false);
    mDropLbl->setColour (Label::backgroundColourId, Colour (0x34ff1c1c));
    mDropLbl->setColour (TextEditor::textColourId, Colours::black);
    mDropLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (mBitDepthLbl = new Label (String(),
                                                 TRANS("Bits per sample:")));
    mBitDepthLbl->setFont (Font (Font::getDefaultMonospacedFontName(), 15.00f, Font::plain));
    mBitDepthLbl->setJustificationType (Justification::centred);
    mBitDepthLbl->setEditable (false, false, false);
    mBitDepthLbl->setColour (TextEditor::textColourId, Colours::black);
    mBitDepthLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (mOutputGainLbl = new Label (String(),
                                                   TRANS("Output gain:")));
    mOutputGainLbl->setFont (Font (Font::getDefaultMonospacedFontName(), 15.00f, Font::plain));
    mOutputGainLbl->setJustificationType (Justification::centred);
    mOutputGainLbl->setEditable (false, false, false);
    mOutputGainLbl->setColour (TextEditor::textColourId, Colours::black);
    mOutputGainLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));


    //[UserPreSize]
    //[/UserPreSize]

    setSize (500, 300);


    //[Constructor] You can add your own custom stuff here..
  auto * sa = new AudioProcessorValueTreeState::SliderAttachment(state,
                                                                 "outputGain",
                                                                 *mSlGain);
  mSlAttachments.add( sa );
  sa = new AudioProcessorValueTreeState::SliderAttachment(state, "bitDepth",
                                                                 *mSlBitDepth);
  mSlAttachments.add( sa );
    //[/Constructor]
}

BRedProcessorEditor::~BRedProcessorEditor()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
  mSlAttachments.clear();
    //[/Destructor_pre]

    mTitleLbl = nullptr;
    mSlGain = nullptr;
    mSlBitDepth = nullptr;
    mDropLbl = nullptr;
    mBitDepthLbl = nullptr;
    mOutputGainLbl = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void BRedProcessorEditor::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colours::white);

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void BRedProcessorEditor::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    mTitleLbl->setBounds (0, 0, getWidth() - 0, 59);
    mSlGain->setBounds (getWidth() - 168, getHeight() - 10 - 56, 168, 56);
    mSlBitDepth->setBounds (0, 134, getWidth() - 0, 112);
    mDropLbl->setBounds (0, 59, getWidth() - 0, 24);
    mBitDepthLbl->setBounds (0, 104, getWidth() - 0, 24);
    mOutputGainLbl->setBounds (getWidth() - 168, getHeight() - 66 - 24, 168, 24);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="BRedProcessorEditor" componentName=""
                 parentClasses="public AudioProcessorEditor" constructorParams="AudioProcessor* processor, AudioProcessorValueTreeState&amp; state"
                 variableInitialisers="AudioProcessorEditor(processor)" snapPixels="8"
                 snapActive="1" snapShown="1" overlayOpacity="0.330" fixedSize="0"
                 initialWidth="500" initialHeight="300">
  <BACKGROUND backgroundColour="ffffffff"/>
  <LABEL name="" id="e1219266aa0b83dd" memberName="mTitleLbl" virtualName=""
         explicitFocusOrder="0" pos="0 0 0M 59" bkgCol="ff4a4a4a" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="BRed - Bit Reducer"
         editableSingleClick="0" editableDoubleClick="0" focusDiscardsChanges="0"
         fontname="Default font" fontsize="30" bold="1" italic="0" justification="36"/>
  <SLIDER name="gainSlider" id="b19e7f5e72dae06f" memberName="mSlGain"
          virtualName="" explicitFocusOrder="0" pos="0Rr 10Rr 168 56" tooltip="Output gain %"
          min="0" max="10" int="0" style="RotaryVerticalDrag" textBoxPos="TextBoxBelow"
          textBoxEditable="0" textBoxWidth="80" textBoxHeight="20" skewFactor="1"
          needsCallback="0"/>
  <SLIDER name="bitDepthSlider" id="e0204d075bc40385" memberName="mSlBitDepth"
          virtualName="" explicitFocusOrder="0" pos="0 134 0M 112" tooltip="Bits per sample"
          min="0" max="10" int="0" style="RotaryVerticalDrag" textBoxPos="TextBoxBelow"
          textBoxEditable="0" textBoxWidth="80" textBoxHeight="20" skewFactor="1"
          needsCallback="0"/>
  <LABEL name="" id="93935fb809778ead" memberName="mDropLbl" virtualName=""
         explicitFocusOrder="0" pos="0 59 0M 24" bkgCol="34ff1c1c" edTextCol="ff000000"
         edBkgCol="0" labelText="Drop a file in this window to play!"
         editableSingleClick="0" editableDoubleClick="0" focusDiscardsChanges="0"
         fontname="Default sans-serif font" fontsize="20" bold="0" italic="1"
         justification="36"/>
  <LABEL name="" id="adeee3683f09db78" memberName="mBitDepthLbl" virtualName=""
         explicitFocusOrder="0" pos="0 104 0M 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Bits per sample:" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default monospaced font"
         fontsize="15" bold="0" italic="0" justification="36"/>
  <LABEL name="" id="63737f4e5bd21334" memberName="mOutputGainLbl" virtualName=""
         explicitFocusOrder="0" pos="0Rr 66Rr 168 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Output gain:" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default monospaced font"
         fontsize="15" bold="0" italic="0" justification="36"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
